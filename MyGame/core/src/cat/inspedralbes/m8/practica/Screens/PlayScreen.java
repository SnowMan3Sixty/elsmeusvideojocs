package cat.inspedralbes.m8.practica.Screens;

import java.util.concurrent.LinkedBlockingQueue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.inspedralbes.m8.practica.MyGame;
import cat.inspedralbes.m8.practica.Scenes.Hud;
import cat.inspedralbes.m8.practica.Sprites.Player;
import cat.inspedralbes.m8.practica.Sprites.Enemies.Enemy;
import cat.inspedralbes.m8.practica.Sprites.Items.Item;
import cat.inspedralbes.m8.practica.Sprites.Items.ItemDefinition;
import cat.inspedralbes.m8.practica.Sprites.Items.Mushroom;
import cat.inspedralbes.m8.practica.Tools.B2WorldCreator;
import cat.inspedralbes.m8.practica.Tools.WorldContactListener;

public class PlayScreen implements Screen{
	
	private MyGame game;
	private TextureAtlas atlas;
	
	private OrthographicCamera gameCam;
	private Viewport gamePort;
	private Hud hud;
	
	//Tiled map variables
	private TmxMapLoader mapLoader;
	private TiledMap map;
	private OrthogonalTiledMapRenderer renderer;
	
	//Box2d variables
	private World world;
	private Box2DDebugRenderer b2dr;
	private B2WorldCreator creator;
	
	private Player player;
	
	private Music music;
	
	//Item variables
	private Array<Item> items;
	private LinkedBlockingQueue<ItemDefinition> itemsToSpawn;
	
	public PlayScreen(MyGame game) {
		this.game = game;
		
		atlas = new TextureAtlas("Player_Enemies.pack");
		
		//camera that will follow the player
		gameCam = new OrthographicCamera();
		//FitViewport to maintain virtual aspect ratio despite screen size
		gamePort = new FitViewport(MyGame.worldWidth / MyGame.PPM, MyGame.worldHeight / MyGame.PPM, gameCam);
		//virtual hud
		hud = new Hud(game.batch);
		
		//Load map
		mapLoader = new TmxMapLoader();
		map = mapLoader.load("level1.tmx");
		renderer = new OrthogonalTiledMapRenderer(map,1 / MyGame.PPM);
		//inicialization of the camera
		gameCam.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2,0);
		
		world = new World(new Vector2(0,-10),true);
		b2dr = new Box2DDebugRenderer();
		
		player = new Player(this);
		
		creator = new B2WorldCreator(this);	
		
		world.setContactListener(new WorldContactListener());
		
		music = MyGame.manager.get("audio/music/level1_music.ogg", Music.class);
		music.setLooping(true);
		music.play();
		
		items = new Array<Item>();
		itemsToSpawn = new LinkedBlockingQueue<ItemDefinition>();		
		
	}
	
	public void spawnItem(ItemDefinition itemDef) {
		itemsToSpawn.add(itemDef);
	}
	
	public void handleSpawningItems() {
		if(!itemsToSpawn.isEmpty()) {
			ItemDefinition itemDef = itemsToSpawn.poll();
			if(itemDef.type == Mushroom.class) {
				items.add(new Mushroom(this,itemDef.position.x,itemDef.position.y));
			}
		}
	}
	
	public TextureAtlas getAtlas() {
		return atlas;		
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}
	public void input(float dt) {
		if(player.currentState != Player.State.DEAD) {
			if(player.b2body.getLinearVelocity().y == 0) {
				if(Gdx.input.isKeyJustPressed(Keys.UP)) {
					  player.b2body.applyLinearImpulse(new Vector2(0,4f), player.b2body.getWorldCenter(), true);
				  }
			}		  
		  if(Gdx.input.isKeyPressed(Keys.RIGHT) && (player.b2body.getLinearVelocity().x <= 2)) {
			  player.b2body.applyLinearImpulse(new Vector2(0.1f,0), player.b2body.getWorldCenter(), true);
		  }
		  if(Gdx.input.isKeyPressed(Keys.LEFT) && (player.b2body.getLinearVelocity().x >= -2)) {
			  player.b2body.applyLinearImpulse(new Vector2(-0.1f,0), player.b2body.getWorldCenter(), true);
		  }
		}
	}
	
	public void update(float dt) {
		input(dt);
		handleSpawningItems();
		
		//takes 1 step in the physics simulation ( 60 times per second)
		world.step(1/60f, 6, 2);
		
		player.update(dt);
		
		//creating the goombas
		for(Enemy enemy : creator.getEnemies()) {
			enemy.update(dt);
			if(enemy.getX() < player.getX() + 224 / MyGame.PPM) {
				enemy.b2body.setActive(true);
			}
		}
		
		//Updating the items
		for(Item item : items) {
			item.update(dt);
		}
		
		hud.update(dt);
		
		if(player.getY() <= 0 && player.currentState != Player.State.DEAD) {
			player.hit(null);
		}
		
		//The camera will follow the player
		if(player.currentState != Player.State.DEAD) {
			gameCam.position.x = player.b2body.getPosition().x;
		}
		
		gameCam.update();
		//the renderer will draw only what the camera can see
		renderer.setView(gameCam);
	}
	
	@Override
	public void render(float delta) {
		//separate out update logic from render
		update(delta);
		
		//clear the game screen with black
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		//render our game map
		renderer.render();
		
		//renderer our Box2DDebugLines
		b2dr.render(world, gameCam.combined);
		
		game.batch.setProjectionMatrix(gameCam.combined);
		game.batch.begin();
		player.draw(game.batch);
		
		//draw goombas
		for(Enemy enemy : creator.getEnemies()) {
			enemy.draw(game.batch);
		}
		
		//draw items
		for(Item item : items) {
			item.draw(game.batch);
		}
		
		game.batch.end();
		
		//set our batch to now draw what the hud camera sees
		game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
		hud.stage.draw();
		
		if(gameOver()) {
			game.setScreen(new GameOverScreen(game));
			dispose();
		}
		
		if(gameWin()) {
			game.setScreen(new WelcomeScreen(game));
			dispose();
		}
	}

	public boolean gameOver() {		
		if(player.currentState == Player.State.DEAD && player.getStateTimer() > 3) {
			return true;
		}
		return false;
	}
	
	public boolean gameWin() {
		if(player.currentState == Player.State.VICTORY && player.getStateTimer() > 6) {
			return true;
		}
		return false;
	}	
	
	@Override
	public void resize(int width, int height) {
		gamePort.update(width, height);
	}
	
	public TiledMap getMap() {
		return map;
	}
	
	public World getWorld() {
		return world;
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		map.dispose();
		renderer.dispose();
		world.dispose();
		b2dr.dispose();
		hud.dispose();
	}

}
