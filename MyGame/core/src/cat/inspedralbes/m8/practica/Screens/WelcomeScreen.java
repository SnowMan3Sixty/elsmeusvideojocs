package cat.inspedralbes.m8.practica.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.inspedralbes.m8.practica.MyGame;

public class WelcomeScreen implements Screen {

	private Viewport viewport;
	private Stage stage;
	
	private Game game;
	
	public WelcomeScreen(Game game) {
		this.game = game;
		viewport = new FitViewport(MyGame.worldWidth,MyGame.worldHeight, new OrthographicCamera());
		stage = new Stage(viewport,((MyGame) game).batch);
		
		Label.LabelStyle font = new Label.LabelStyle(new BitmapFont(),Color.WHITE);
		
		Table table = new Table();
		table.center();
		table.setFillParent(true);
		
		Label welcomeLabel = new Label("WELCOME TO: ",font);
		Label titleLabel = new Label("DEFINITELY NOT MARIO BROS",font);
		Label playLabel = new Label("Click to Start Playing",font);
		
		table.add(welcomeLabel).expandX();
		table.row();
		table.add(titleLabel).expandX().padTop(10f);
		table.row();
		table.add(playLabel).expandX().padTop(10f);
		
		stage.addActor(table);

	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		if(Gdx.input.justTouched()) {
			game.setScreen(new PlayScreen((MyGame) game));
			dispose();
		}
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.draw();			
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();		
	}

}
