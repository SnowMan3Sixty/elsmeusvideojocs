package cat.inspedralbes.m8.practica.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import cat.inspedralbes.m8.practica.MyGame;
import cat.inspedralbes.m8.practica.Sprites.Player;
import cat.inspedralbes.m8.practica.Sprites.Enemies.Enemy;
import cat.inspedralbes.m8.practica.Sprites.Items.Item;
import cat.inspedralbes.m8.practica.Sprites.TileObjects.InteractiveTileObject;

public class WorldContactListener implements ContactListener{

	@Override
	public void beginContact(Contact contact) {
		Fixture fixA = contact.getFixtureA();
		Fixture fixB = contact.getFixtureB();
		
		int colissionDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;
		
		switch(colissionDef) {
			case MyGame.PLAYER_HEAD_BIT | MyGame.BRICK_BIT:				
			case MyGame.PLAYER_HEAD_BIT | MyGame.COIN_BIT:
				if(fixA.getFilterData().categoryBits == MyGame.PLAYER_HEAD_BIT) {
					((InteractiveTileObject) fixB.getUserData()).onHeadHit((Player) fixA.getUserData());
				}
				else {
					((InteractiveTileObject) fixA.getUserData()).onHeadHit((Player) fixB.getUserData());
				}
				break;
			case MyGame.ENEMY_HEAD_BIT | MyGame.PLAYER_BIT:
				//Then we know fixA is the enemy
				if(fixA.getFilterData().categoryBits == MyGame.ENEMY_HEAD_BIT) {
//					((Enemy) fixA.getUserData()).hitOnHead((Player) fixB.getUserData());
				}else {
//					((Enemy) fixB.getUserData()).hitOnHead((Player) fixA.getUserData());
				}
				break;
			case MyGame.ENEMY_BIT | MyGame.OBJECT_BIT:
				if(fixA.getFilterData().categoryBits == MyGame.ENEMY_BIT) {
					((Enemy) fixA.getUserData()).reverseVelocity(true,false);
				}else {
					((Enemy) fixB.getUserData()).reverseVelocity(true, false);
				}
				break;
			case MyGame.PLAYER_BIT | MyGame.ENEMY_BIT:
				Gdx.app.log("PLAYER","DIED");
				if(fixA.getFilterData().categoryBits == MyGame.PLAYER_BIT) {
//					((Player) fixA.getUserData()).hit((Enemy) fixB.getUserData());
				}
				else {
//					((Player) fixB.getUserData()).hit((Enemy) fixA.getUserData());
				}
				break;
			case MyGame.ENEMY_BIT | MyGame.ENEMY_BIT:
				((Enemy) fixA.getUserData()).onEnemyHit((Enemy) fixB.getUserData());
				((Enemy) fixB.getUserData()).onEnemyHit((Enemy) fixA.getUserData());
				break;
			case MyGame.ITEM_BIT | MyGame.OBJECT_BIT:
				if(fixA.getFilterData().categoryBits == MyGame.ENEMY_BIT) {
					((Item) fixA.getUserData()).reverseVelocity(true,false);
				}else {
					((Item) fixB.getUserData()).reverseVelocity(true, false);
				}
				break;
			case MyGame.ITEM_BIT | MyGame.PLAYER_BIT:
				if(fixA.getFilterData().categoryBits == MyGame.ENEMY_BIT) {
					((Item) fixA.getUserData()).use((Player) fixB.getUserData());
				}else {
					if( !(fixA.getUserData() instanceof String)){
						((Item) fixB.getUserData()).use((Player) fixA.getUserData());
					}					
				}
				break;
		}
	}

	@Override
	public void endContact(Contact contact) {
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

}
