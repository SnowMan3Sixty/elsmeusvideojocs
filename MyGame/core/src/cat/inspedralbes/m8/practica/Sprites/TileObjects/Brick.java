package cat.inspedralbes.m8.practica.Sprites.TileObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;

import cat.inspedralbes.m8.practica.MyGame;
import cat.inspedralbes.m8.practica.Scenes.Hud;
import cat.inspedralbes.m8.practica.Screens.PlayScreen;
import cat.inspedralbes.m8.practica.Sprites.Player;

public class Brick extends InteractiveTileObject{

	public Brick(PlayScreen screen, MapObject object) {
		super(screen, object);
		fixture.setUserData(this);
		setCategoryFilter(MyGame.BRICK_BIT);
	}

	@Override
	public void onHeadHit(Player player) {
		Gdx.app.log("Brick","Collision");
		
		if(player.isBig()) {		
			setCategoryFilter(MyGame.DESTROYED_BIT);
			getCell().setTile(null);
			Hud.addScore(200);
			MyGame.manager.get("audio/sounds/breakblock.wav",Sound.class).play();
		}
		else {
			MyGame.manager.get("audio/sounds/bump.wav",Sound.class).play();
		}
		
	}
}
