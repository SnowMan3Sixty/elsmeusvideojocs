package cat.inspedralbes.m8.practica.Sprites;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import cat.inspedralbes.m8.practica.MyGame;
import cat.inspedralbes.m8.practica.Screens.PlayScreen;
import cat.inspedralbes.m8.practica.Sprites.Enemies.Enemy;
import cat.inspedralbes.m8.practica.Sprites.Enemies.Turtle;

public class Player extends Sprite{
	
	//player state variables
	public enum State { FALLING , JUMPING, STANDING, RUNNING, GROWING, DEAD,VICTORY};
	public State currentState;
	public State previousState;
	public World world;
	public Body b2body;
	//player animation variables
	private TextureRegion playerStand;
	private Animation<TextureRegion> playerRun;
	private TextureRegion playerJump;
	private float stateTimer;
	//to know to which side is running the player
	private boolean runningRight;
	private boolean playerIsBig;
	private boolean runGrowAnimation;
	private boolean timeToDefineBigPlayer;
	private boolean timeToRedefinePlayer;
	
	//big player variables
	private TextureRegion bigPlayerStand;
	private TextureRegion bigPlayerJump;
	private Animation<TextureRegion> bigPlayerRun;	
	private Animation<TextureRegion> growPlayer;
	
	private TextureRegion playerDead;
	private boolean playerIsDead;
	
	private boolean playerWin;
	
	
	public Player(PlayScreen screen) {		
		this.world = screen.getWorld();
		currentState = State.STANDING;
		previousState = State.STANDING;
		stateTimer = 0;
		runningRight = true;	
		playerWin = false;

		
		Array<TextureRegion> frames = new Array<TextureRegion>();
		// Little player running animation
		for(int i = 1; i < 4; i++) {
			frames.add(new TextureRegion(screen.getAtlas().findRegion("little_mario"), i * 16, 0, 16, 16));
		}
		playerRun = new Animation<TextureRegion>(0.1f,frames);
		frames.clear();
		
		//Big Player running animation
		for(int i = 1; i < 4; i++) {
			frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), i * 16, 0, 16, 32));
		}
		bigPlayerRun = new Animation<TextureRegion>(0.1f,frames);
		frames.clear();
		
		//get set animation frames from growing player
		frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),240,0,16,32));
		frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32));
		frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),240,0,16,32));
		frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32));
		growPlayer = new Animation<TextureRegion>(0.2f,frames);
		
		//get jump animation frames and add the to the playerJump Animation
		playerJump = new TextureRegion(screen.getAtlas().findRegion("little_mario"),80,0,16,16);
		bigPlayerJump = new TextureRegion(screen.getAtlas().findRegion("big_mario"),80,0,16,32);
		
		playerStand = new TextureRegion(screen.getAtlas().findRegion("little_mario"),0,0,16,16);		
		bigPlayerStand = new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32);
		
		//create dead player texture
		playerDead = new TextureRegion(screen.getAtlas().findRegion("little_mario"),96,0,16,16);
		
		definePlayer();
		setBounds(0, 0, 16 / MyGame.PPM, 16 / MyGame.PPM);
		setRegion(playerStand);
	}
	
	public void update(float dt) {
		if(playerIsBig) {
			setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2 - 6 / MyGame.PPM);
		}
		else {
			setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2 );
		}
		
		setRegion(getFrame(dt));
		if(timeToDefineBigPlayer) {
			defineBigPlayer();
		}
		if(timeToRedefinePlayer) {
			redefinePlayer();
		}
	}
	
	public TextureRegion getFrame(float dt) {
		currentState = getState();
		
		TextureRegion region;
		switch(currentState) {
			case DEAD:
				region = playerDead;
				break;
			case GROWING:
				region = growPlayer.getKeyFrame(stateTimer);
				if(growPlayer.isAnimationFinished(stateTimer)) {
					runGrowAnimation = false;
				}
				break;
			case JUMPING:
				region = playerIsBig ? bigPlayerJump : playerJump;
				break;
			case RUNNING:
				region = playerIsBig ? (TextureRegion) bigPlayerRun.getKeyFrame(stateTimer,true) : (TextureRegion) playerRun.getKeyFrame(stateTimer,true);
				break;
			case FALLING:
			case STANDING:
				default:
					region = playerIsBig ? bigPlayerStand : playerStand;
					break;
		}
		if((b2body.getLinearVelocity().x < 0 || !runningRight) && !region.isFlipX()) {
//			region.flip(true, false);
			runningRight = false;
		}
		else if((b2body.getLinearVelocity().x > 0 || runningRight) && region.isFlipX()) {
//			region.flip(true, false);
			runningRight = true;
		}
		
		stateTimer = currentState == previousState ? stateTimer + dt : 0;
		previousState = currentState;
		return region;
	}
	
	//Returns the state of the player
	public State getState() {
		if(playerWin) {
			return State.STANDING;
//			return State.VICTORY;
		}
		else if(playerIsDead) {
			return State.STANDING;
//			return State.DEAD;
		}
		else if(runGrowAnimation) {
			return State.STANDING;
//			return State.GROWING;
		}
		else if(b2body.getLinearVelocity().y > 0 || (b2body.getLinearVelocity().y < 0 && previousState == State.JUMPING)) {
			return State.STANDING;
//			return State.JUMPING;
		}
		else if(b2body.getLinearVelocity().y < 0) {
			return State.STANDING;
//			return State.FALLING;
		}
		else if(b2body.getLinearVelocity().x != 0) {
			return State.STANDING;
//			return State.RUNNING;
		}
		else
			return State.STANDING;
	}
	
	public void grow() {
		if(playerIsBig) {
			timeToDefineBigPlayer = false;
		}else {		
			runGrowAnimation = true;
			playerIsBig = true;
			timeToDefineBigPlayer = true;
			setBounds(getX(), getY(), getWidth(), getHeight() * 2);
			MyGame.manager.get("audio/sounds/powerup_growing.wav",Sound.class).play();
		}
	}
	
	public boolean isDead() {
		return playerIsDead;
	}
	
	public float getStateTimer() {
		return stateTimer;
	}
	
	 public boolean isBig() {
	       return playerIsBig;
	 }
	 
	 public void setPlayerWin(boolean win) {
		 this.playerWin = win;
	 }
	 
	 public void hit(Enemy enemy) {
		 if(enemy instanceof Turtle && ((Turtle) enemy).getCurrentState() == Turtle.State.STANDING_SHELL) {
			 ((Turtle) enemy).kick(this.getX() <= enemy.getX() ? Turtle.KICK_RIGHT_SPEED : Turtle.KICK_LEFT_SPEED);
		 }
		 else {
			 if(playerIsBig) {
				 playerIsBig = false;
				 timeToRedefinePlayer = true;
				 setBounds(getX(), getY(), getWidth(), getHeight() / 2);
				 MyGame.manager.get("audio/sounds/power_down.wav",Sound.class).play();
			 }else {
				 MyGame.manager.get("audio/music/level1_music.ogg",Music.class).stop();
				 MyGame.manager.get("audio/sounds/playerdie.wav",Sound.class).play();
				 playerIsDead = true;
				 Filter filter = new Filter();
				 filter.maskBits = MyGame.NOTHING_BIT;
				 for(Fixture fixture : b2body.getFixtureList()) {
					 fixture.setFilterData(filter);
				 }
				 b2body.applyLinearImpulse(new Vector2(0,4f),b2body.getWorldCenter(), true);
			 }	
		 }			
	 }
	 
	 public void redefinePlayer() {
		 
		 Vector2 position = b2body.getPosition();
		 world.destroyBody(b2body);
		 
		 BodyDef bdef = new BodyDef();
		 bdef.position.set(position);
		 bdef.type = BodyDef.BodyType.DynamicBody;
		 b2body = world.createBody(bdef);
		 
		 FixtureDef fdef = new FixtureDef();
		 CircleShape shape = new CircleShape();
		 shape.setRadius(6 / MyGame.PPM);
		 
		 //set category of the player
		 fdef.filter.categoryBits = MyGame.PLAYER_BIT;
		 //set with what the player can collide
		 fdef.filter.maskBits = MyGame.GROUND_BIT |
				 MyGame.COIN_BIT |
				 MyGame.BRICK_BIT |
				 MyGame.ENEMY_BIT |
				 MyGame.OBJECT_BIT |
				 MyGame.ENEMY_HEAD_BIT |
				 MyGame.ITEM_BIT;
		
		 fdef.shape = shape;
		 b2body.createFixture(fdef).setUserData(this);
		
		 EdgeShape head = new EdgeShape();
		 head.set(new Vector2(-2 / MyGame.PPM, 6 / MyGame.PPM),new Vector2(2 / MyGame.PPM,6 / MyGame.PPM));
		 fdef.filter.categoryBits = MyGame.PLAYER_HEAD_BIT;
		 fdef.shape = head;
		 fdef.isSensor = true;
		
		 b2body.createFixture(fdef).setUserData(this);
		 
		 timeToRedefinePlayer = false;
	 }
	
	public void defineBigPlayer() {
		Vector2 currentPosition = b2body.getPosition();
		world.destroyBody(b2body);
		
		BodyDef bdef = new BodyDef();
		bdef.position.set(currentPosition.add(0,10 / MyGame.PPM));
		bdef.type = BodyDef.BodyType.DynamicBody;
		b2body = world.createBody(bdef);
		
		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(6 / MyGame.PPM);
		
		//set category of the player
		fdef.filter.categoryBits = MyGame.PLAYER_BIT;
		//set with what the player can collide
		fdef.filter.maskBits = MyGame.GROUND_BIT |
				MyGame.COIN_BIT |
				MyGame.BRICK_BIT |
				MyGame.ENEMY_BIT |
				MyGame.OBJECT_BIT |
				MyGame.ENEMY_HEAD_BIT |
				MyGame.ITEM_BIT;
		
		fdef.shape = shape;
		b2body.createFixture(fdef).setUserData(this);
		shape.setPosition(new Vector2(0,-14 / MyGame.PPM));
		b2body.createFixture(fdef).setUserData(this);
		
		EdgeShape head = new EdgeShape();
		head.set(new Vector2(-2 / MyGame.PPM, 6 / MyGame.PPM),new Vector2(2 / MyGame.PPM,6 / MyGame.PPM));
		fdef.filter.categoryBits = MyGame.PLAYER_HEAD_BIT;
		fdef.shape = head;
		fdef.isSensor = true;
		
		b2body.createFixture(fdef).setUserData(this);
		
		timeToDefineBigPlayer = false;
	}
	
	public void definePlayer() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(32 / MyGame.PPM,32 / MyGame.PPM);
		bdef.type = BodyDef.BodyType.DynamicBody;
		b2body = world.createBody(bdef);
		
		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(6 / MyGame.PPM);
		
		//set category of the player
		fdef.filter.categoryBits = MyGame.PLAYER_BIT;
		//set with what the player can collide
		fdef.filter.maskBits = MyGame.GROUND_BIT |
				MyGame.COIN_BIT |
				MyGame.BRICK_BIT |
				MyGame.ENEMY_BIT |
				MyGame.OBJECT_BIT |
				MyGame.ENEMY_HEAD_BIT |
				MyGame.ITEM_BIT;
		
		fdef.shape = shape;
		b2body.createFixture(fdef).setUserData(this);
		
		EdgeShape head = new EdgeShape();
		head.set(new Vector2(-2 / MyGame.PPM, 6 / MyGame.PPM),new Vector2(2 / MyGame.PPM,6 / MyGame.PPM));
		fdef.filter.categoryBits = MyGame.PLAYER_HEAD_BIT;
		fdef.shape = head;
		fdef.isSensor = true;
		
		b2body.createFixture(fdef).setUserData(this);
	}
}
