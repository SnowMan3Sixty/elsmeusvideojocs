package cat.inspedralbes.m8.practica.Sprites.TileObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Vector2;

import cat.inspedralbes.m8.practica.MyGame;
import cat.inspedralbes.m8.practica.Scenes.Hud;
import cat.inspedralbes.m8.practica.Screens.PlayScreen;
import cat.inspedralbes.m8.practica.Sprites.Player;
import cat.inspedralbes.m8.practica.Sprites.Items.ItemDefinition;
import cat.inspedralbes.m8.practica.Sprites.Items.Mushroom;


public class Coin extends InteractiveTileObject {
	private static TiledMapTileSet tileSet;
	private final int BLANK_COIN = 28;
	
	public Coin(PlayScreen screen, MapObject object) {
		super(screen, object);		
		tileSet = map.getTileSets().getTileSet("NES - Super Mario Bros - Tileset");
		fixture.setUserData(this);
		setCategoryFilter(MyGame.COIN_BIT);
	}

	@Override
	public void onHeadHit(Player player) {
		Gdx.app.log("Coin","Collision");
		if(getCell().getTile().getId() == BLANK_COIN) {
			MyGame.manager.get("audio/sounds/bump.wav",Sound.class).play();
		}
		else {
			if(object.getProperties().containsKey("mushroom")) {
				screen.spawnItem(new ItemDefinition(new Vector2(body.getPosition().x, body.getPosition().y + 16 / MyGame.PPM),Mushroom.class));
				MyGame.manager.get("audio/sounds/mushroom_appears.wav",Sound.class).play();
			}
			else if(object.getProperties().containsKey("victory")) {
				MyGame.manager.get("audio/music/level1_music.ogg",Music.class).stop();
				MyGame.manager.get("audio/music/world_clear.wav",Music.class).play();
				player.setPlayerWin(true);
			}
			else {
				MyGame.manager.get("audio/sounds/coin.wav",Sound.class).play();
			}			
		}
		getCell().setTile(tileSet.getTile(BLANK_COIN));
		Hud.addScore(100);
	}

}
