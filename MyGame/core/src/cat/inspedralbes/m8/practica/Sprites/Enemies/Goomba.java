package cat.inspedralbes.m8.practica.Sprites.Enemies;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;

import cat.inspedralbes.m8.practica.MyGame;
import cat.inspedralbes.m8.practica.Scenes.Hud;
import cat.inspedralbes.m8.practica.Screens.PlayScreen;
import cat.inspedralbes.m8.practica.Sprites.Player;

public class Goomba extends Enemy {

	private float stateTime;
	private Animation<TextureRegion> walkAnimation;
	private Array<TextureRegion> frames;
	private boolean setToDestroy;
	private boolean destroyed;
	
	public Goomba(PlayScreen screen, float x, float y) {
		super(screen, x, y);
		frames = new Array<TextureRegion>();
		for(int i = 0; i < 2; i++) {
			frames.add(new TextureRegion(screen.getAtlas().findRegion("goomba"), i * 16, 0 ,16,16));			
		}
		walkAnimation = new Animation<TextureRegion>(0.4f,frames);
		stateTime = 0;
		setBounds(getX(), getY(), 16 / MyGame.PPM, 16 / MyGame.PPM);
		setToDestroy = false;
		destroyed = false;
	}
	
	public void update(float dt) {
		stateTime += dt;
		
		if(setToDestroy && !destroyed) {
			world.destroyBody(b2body);
			destroyed = true;
			setRegion(new TextureRegion(screen.getAtlas().findRegion("goomba"),32,0,16,16));
			//To know how many time from the goomba dead
			stateTime = 0;
		}
		else if(!destroyed) {
			b2body.setLinearVelocity(velocity);
			
			setPosition(b2body.getPosition().x - getWidth() /2, b2body.getPosition().y - getHeight() / 2);
			setRegion(walkAnimation.getKeyFrame(stateTime,true));
		}		
	}

	@Override
	protected void defineEnemy() {
		BodyDef bdef = new BodyDef();
		//This change the position where goomba is spawned
		bdef.position.set(getX(),getY());
		
		bdef.type = BodyDef.BodyType.DynamicBody;
		b2body = world.createBody(bdef);
		
		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(6 / MyGame.PPM);
		
		//set category of the player
		fdef.filter.categoryBits = MyGame.ENEMY_BIT;
		//set with what the player can collide
		fdef.filter.maskBits = MyGame.GROUND_BIT | 
				MyGame.COIN_BIT |
				MyGame.BRICK_BIT |
				MyGame.ENEMY_BIT |
				MyGame.OBJECT_BIT |
				MyGame.PLAYER_BIT;
		
		fdef.shape = shape;
		b2body.createFixture(fdef).setUserData(this); 
		
		//Goomba head to know when player hits its head
		PolygonShape head = new PolygonShape();
		Vector2[] vertice = new Vector2[4];
		vertice[0] = new Vector2(-5,8).scl(1 / MyGame.PPM);
		vertice[1] = new Vector2(5,8).scl(1 / MyGame.PPM);
		vertice[2] = new Vector2(-3,3).scl(1 / MyGame.PPM);
		vertice[3] = new Vector2(3,3).scl(1 / MyGame.PPM);
		head.set(vertice);
		
		fdef.shape = head;
		//player bounce 50% when jumping on the head
		fdef.restitution = 0.5f;
		fdef.filter.categoryBits = MyGame.ENEMY_HEAD_BIT;
		//Setting the user data gives us acces to the goomba class from the b2body
		b2body.createFixture(fdef).setUserData(this);	
		
	}

	public void draw(Batch batch) {
		if(!destroyed || stateTime < 1) {
			super.draw(batch);
		}
	}
	
	@Override
	public void hitOnHead(Player player) {		
		setToDestroy = true;
		MyGame.manager.get("audio/sounds/stomp.wav",Sound.class).play();
		Hud.addScore(100);
	}
	
	@Override
	public void onEnemyHit(Enemy enemy) {
		if(enemy instanceof Turtle && ((Turtle) enemy).currentState == Turtle.State.MOVING_SHELL) {
			setToDestroy = true;			
		}
		else {
			reverseVelocity(true, false);
		}
		
	}

}
