package cat.inspedralbes.m8.practica.Sprites.Items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import cat.inspedralbes.m8.practica.MyGame;
import cat.inspedralbes.m8.practica.Screens.PlayScreen;
import cat.inspedralbes.m8.practica.Sprites.Player;

public class Mushroom extends Item{

	public Mushroom(PlayScreen screen, float x, float y) {
		super(screen, x, y);
		setRegion(screen.getAtlas().findRegion("mushroom"),0,0,16,16);
		velocity = new Vector2(1f,-2f);
	}

	@Override
	public void defineItem() {
		BodyDef bdef = new BodyDef();
		//This change the position where mushroom is spawned
		bdef.position.set(getX(),getY());
		
		bdef.type = BodyDef.BodyType.DynamicBody;
		body = world.createBody(bdef);
		
		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(6 / MyGame.PPM);
		
		//Setting the type of filter for the mushroom 
		// + the objects with what can colide
		fdef.filter.categoryBits = MyGame.ITEM_BIT;
		fdef.filter.maskBits = MyGame.PLAYER_BIT |
				MyGame.OBJECT_BIT |
				MyGame.GROUND_BIT | 
				MyGame.COIN_BIT |
				MyGame.BRICK_BIT;
		
		fdef.shape = shape;
		body.createFixture(fdef).setUserData(this); 
		
	}

	@Override
	public void use(Player player) {
		destroy();
//		player.grow();		
	}
	
	@Override
	public void update(float dt) {		
		super.update(dt);
		setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2 );
		body.setLinearVelocity(velocity);
		velocity.y = body.getLinearVelocity().y;
		body.setLinearVelocity(velocity);
	}

}
