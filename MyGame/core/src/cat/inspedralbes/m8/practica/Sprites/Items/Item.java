package cat.inspedralbes.m8.practica.Sprites.Items;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

import cat.inspedralbes.m8.practica.MyGame;
import cat.inspedralbes.m8.practica.Screens.PlayScreen;
import cat.inspedralbes.m8.practica.Sprites.Player;

public abstract class Item extends Sprite{
	
	protected PlayScreen screen;
	protected World world;
	protected Vector2 velocity;
	protected boolean toDestroy;
	protected boolean destroyed;
	protected Body body;
	
	public Item(PlayScreen screen, float x, float y) {
		this.screen = screen;
		this.world = screen.getWorld();
		setPosition(x, y);
		setBounds(getX(), getY(), 16 / MyGame.PPM, 16 / MyGame.PPM);
		defineItem();
		toDestroy = false;
		destroyed = false;		
	}
	
	public abstract void defineItem();
	public abstract void use(Player player);
	
	public void update(float dt) {
		if(toDestroy && !destroyed) {
			world.destroyBody(body);
			destroyed = true;
		}
	}	
	
	public void draw(Batch batch) {
		if(!destroyed) {
			super.draw(batch);
		}
	}
	
	//We have to destroy a body in the update method to avoid colission problems
	public void destroy() {
		toDestroy = true;
	}
	
	public void reverseVelocity(boolean x, boolean y) {
		if(x) {
			velocity.x = -velocity.x;			
		}
		if(y) {
			velocity.y = -velocity.y;
		}
	}
}
