package cat.inspedralbes.m8.practica;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import cat.inspedralbes.m8.practica.Screens.WelcomeScreen;

public class MyGame extends Game {
	
	public static final int worldWidth = 400;
	public static final int worldHeight = 208;
	
	public static final float PPM = 100;
	
	//Variables of the filters
	public static final short NOTHING_BIT = 0;
	public static final short GROUND_BIT = 1;
	public static final short PLAYER_BIT = 2;
	public static final short BRICK_BIT = 4;
	public static final short COIN_BIT = 8;
	public static final short DESTROYED_BIT = 16;
	public static final short OBJECT_BIT = 32;
	public static final short ENEMY_BIT = 64;
	public static final short ENEMY_HEAD_BIT = 128;
	public static final short ITEM_BIT = 256;
	public static final short PLAYER_HEAD_BIT = 512;
	
	
	public SpriteBatch batch;
	
	public static AssetManager manager;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		manager = new AssetManager();
		manager.load("audio/music/level1_music.ogg",Music.class);
		manager.load("audio/music/world_clear.wav",Music.class);
		
		manager.load("audio/sounds/coin.wav",Sound.class);
		manager.load("audio/sounds/bump.wav",Sound.class);
		manager.load("audio/sounds/breakblock.wav",Sound.class);
		manager.load("audio/sounds/mushroom_appears.wav",Sound.class);
		manager.load("audio/sounds/powerup_growing.wav",Sound.class);
		manager.load("audio/sounds/power_down.wav",Sound.class);
		manager.load("audio/sounds/stomp.wav",Sound.class);
		manager.load("audio/sounds/playerdie.wav",Sound.class);
		
		manager.finishLoading();
		
		setScreen(new WelcomeScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();	
		manager.dispose();
		batch.dispose();
	}
}
