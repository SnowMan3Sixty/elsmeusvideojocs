package cat.inspedralbes.m8.practica.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.inspedralbes.m8.practica.MyGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.title = "Definitely not MarioBros";
		config.width = 780;
		config.height = 408;
		
		new LwjglApplication(new MyGame(), config);
	}
}
