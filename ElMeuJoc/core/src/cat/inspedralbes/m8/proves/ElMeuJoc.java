package cat.inspedralbes.m8.proves;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ElMeuJoc extends Game {
	
	public SpriteBatch batch;
		
	@Override
	public void create () {
		batch = new SpriteBatch();		
		
				
		setScreen(new SplashScreen(this));
		
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}
	
	//Metode cridat continuament
	@Override
	public void render () {
		//Neteja de pantalla
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		//Aquest render crida al render de la classe Game i aquest crida al render de la screen activa
		//necessari perqu� es vegi alguna cosa
		super.render();
	}		
	
	@Override
	public void dispose () {
		batch.dispose();		
	}
}
