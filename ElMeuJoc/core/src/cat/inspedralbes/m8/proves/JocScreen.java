package cat.inspedralbes.m8.proves;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.inspedralbes.m8.proves.enemics.EnemicZigZag;
import cat.inspedralbes.m8.proves.enemics.IEnemic;

public class JocScreen extends ScreenAdapter{
	
	ElMeuJoc joc;
	
	//Creem la camara 
	OrthographicCamera camera;
	Viewport viewport;
	
	//Model de dades
	Rectangle player;
	List<IEnemic> enemics;
	List<Rectangle> disparsJugador;
	float tempsUltimEnemic=0; 
	float tempsUltimDisparJugador = 0; 
	
	//Grafics
	Texture playerTexture;
	Texture enemicTexture;
	Texture disparTexture;
	Texture fonsTexture;
	
	public JocScreen(ElMeuJoc joc) {
		this.joc = joc;
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false,Constants.MON_AMPLE,Constants.MON_ALT);
		viewport = new FitViewport(Constants.MON_AMPLE, Constants.MON_ALT, camera);
		
		player = new Rectangle(0,0,Constants.AMPLE_PLAYER,Constants.ALT_PLAYER);
		enemics = new ArrayList<IEnemic>();
		disparsJugador = new ArrayList<Rectangle>();
		
		playerTexture = new Texture(Gdx.files.internal("naus/player.png"));
		enemicTexture = new Texture(Gdx.files.internal("naus/enemy.png"));
		disparTexture = new Texture(Gdx.files.internal("lasers/laserBlue.png"));
		fonsTexture = new Texture(Gdx.files.internal("backGround.png"));
	}
	
	public void resize(int width, int height) {
        viewport.update(width, height);
    }
	
	@Override
	public void render(float delta) {		
		//1.Gestio input
		gestionarInput();
		
		//2.Calculs enemics + colisions + gesti� lasers
		actualitza();
		
		//3.Dibuixat
		
		//L�nies essencials pel control de la camera
		camera.update();
		joc.batch.setProjectionMatrix(camera.combined);
		
		joc.batch.begin();
		
		//Pintem el background
		joc.batch.draw(fonsTexture,0,0,Constants.MON_AMPLE,Constants.MON_ALT);
		
		joc.batch.draw(playerTexture, player.x, player.y,player.width,player.height);
		
		//Dibuixar enemics
		for(IEnemic enemic : enemics) {
			enemic.dibuixarse(joc.batch);
		}
		
		//Dibuixar lasers
		for(Rectangle dispar : disparsJugador) {
			joc.batch.draw(disparTexture,dispar.x,dispar.y,dispar.width,dispar.height);
		}
		joc.batch.end();
	}
	
	private void actualitza() {
		
		float delta = Gdx.graphics.getDeltaTime();
		
		//Si ha passat un cert temps apareix un enemic
		tempsUltimEnemic += delta;
		if(tempsUltimEnemic > Constants.TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
		
			int x = new Random().nextInt(Constants.MON_AMPLE);
			int y = Constants.MON_ALT;;
			
			IEnemic nouEnemic = (IEnemic) new EnemicZigZag(x, y, Constants.AMPLE_ENEMIC, Constants.ALT_ENEMIC, enemicTexture);
			enemics.add(nouEnemic);
			
			//Reset del comptador;
			tempsUltimEnemic=0;
		}
		
		//Avancem els dispars
		for(Rectangle dispar : disparsJugador) {
			dispar.y += Constants.VELOCITAT_DISPAR_PLAYER*delta;
		}
		
		//Avancem els enemics
		for(IEnemic enemic : enemics) {
			enemic.actualitzarse(delta);
		}
		
		//Mirem colisions amb el player
		for(IEnemic enemic : enemics) {
			if(enemic.solapa(player)) {
				// anar a la SplashScreen
				joc.setScreen(new SplashScreen(joc));
			}
		}
		
		//Mirem colisions del laser del player
		for (Iterator<IEnemic> iterEnemics = enemics.iterator(); iterEnemics.hasNext();) {
			IEnemic enemic = (IEnemic) iterEnemics.next();

			for (Iterator<Rectangle> iterDispar = disparsJugador.iterator(); iterDispar.hasNext();) {
				Rectangle dispar = (Rectangle) iterDispar.next();

				if (enemic.solapa(dispar)) {
					iterDispar.remove();
					iterEnemics.remove();
					break;
				}
			}
		}
		
		//Depuraci�: info enemics
		Gdx.app.debug("Enemics", "Hi ha " + enemics.size() + "  enemics");
		
		//Els enemics que surten de la pantalla per baix, son eliminats de la List
		for(Iterator<IEnemic> iterator = enemics.iterator(); iterator.hasNext();) {
			IEnemic enemic = (IEnemic) iterator.next();
			if(enemic.foraDePantalla()) {
				iterator.remove();
				//Depuracio: eliminat
				Gdx.app.debug("Enemics", "Eliminat");
			}
		}
		//Depuracio: comprovaci�
		Gdx.app.debug("Enemics", "Hi ha " + enemics.size() + "  enemics");
		
		//Eliminem el laser si surt de la pantalla
		for(Iterator<Rectangle> iterator = disparsJugador.iterator();iterator.hasNext();) {
			Rectangle dispar = (Rectangle)  iterator.next();
			if(dispar.y  > Constants.MON_ALT) {
				iterator.remove();
				//Depuracio: laser eliminat
				Gdx.app.debug("Laser", "Eliminat");
			}
		}
	}	
	
	private void gestionarInput() {
		
		float delta = Gdx.graphics.getDeltaTime();
		
		//Moviment de la nau
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			player.x -= Constants.VELOCITAT_NAU_JUGADOR*delta;
		}
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			player.x += Constants.VELOCITAT_NAU_JUGADOR*delta;
		}
		if(Gdx.input.isKeyPressed(Keys.UP)) {
			player.y += Constants.VELOCITAT_NAU_JUGADOR*delta;
		}
		if(Gdx.input.isKeyPressed(Keys.DOWN)) {
			player.y -= Constants.VELOCITAT_NAU_JUGADOR*delta;
		}
		//Nou Dispar
		tempsUltimDisparJugador  += delta;
		if(Gdx.input.isKeyPressed(Keys.SPACE) && tempsUltimDisparJugador > Constants.TEMPS_ENTRE_DISPARS_JUGADOR) {
			
			float x = player.x + player.width/2;
			float y = player.y + player.height;
			disparsJugador.add(new Rectangle(x,y,Constants.AMPLE_LASER,Constants.ALT_LASER));
			
			tempsUltimDisparJugador = 0;
		}
		
		//Colisions amb el mapa
		int amplePantalla =  Gdx.graphics.getWidth();
		int alturaPantalla = Gdx.graphics.getHeight();
		
		if(player.x < 0) {
			player.x = 0;
		}
		if(player.x > (amplePantalla - player.width)) {
			player.x = amplePantalla - player.width;
		}		
		if(player.y < 0) {
			player.y  = 0;
		}
		if(player.y > (alturaPantalla - player.height)) {
			player.y = alturaPantalla - player.height;
		}
	}
	
	@Override
	public void dispose() {
		playerTexture.dispose();
		enemicTexture.dispose();
		disparTexture.dispose();
		fonsTexture.dispose();
	}

}
