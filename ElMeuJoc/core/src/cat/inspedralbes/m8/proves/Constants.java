package cat.inspedralbes.m8.proves;

public class Constants {
	
	//Tots els numeros estan adaptats a les coordinades del mon
	public static final int MON_AMPLE = 1000;
	public static final int MON_ALT = 800;
				
	//Jugador
	public static final float AMPLE_PLAYER=50;
	public static final float ALT_PLAYER=50;
	public static final int VELOCITAT_NAU_JUGADOR = 250; // Pixels per segons
	
	public static final float AMPLE_LASER = 10;
	public static final float ALT_LASER = 50;
	public static final float VELOCITAT_DISPAR_PLAYER = 400;
	public static final int TEMPS_ENTRE_DISPARS_JUGADOR = 1;
	
	//Enemics
	public static final float AMPLE_ENEMIC=50;
	public static final float ALT_ENEMIC=50;
	public static final int TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS = 1;
	public static final int VELOCITAT_NAU_ENEMIC = 150; // Pixels per segons
	public static final float VELOCITAT_HORIZONTAL_NAU_ENEMICZIGZAG = 50;
	
}
