package cat.inspedralbes.m8.proves;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;

public class SplashScreen implements Screen{
	
	//Referencia al joc
	ElMeuJoc joc;
	
	//Imatge de l'empresa
	Texture logo;
	
	float tempsDesDeIniciJoc = 0;
	static int TEMPS_LOGO_S = 3;	
	
	public SplashScreen(ElMeuJoc joc) {
		this.joc = joc;		
		logo = new Texture(Gdx.files.internal("badlogic.jpg"));
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void render(float delta) {		
		tempsDesDeIniciJoc += delta;
		if(tempsDesDeIniciJoc > TEMPS_LOGO_S) {
			//canvi a JocScreen
			//es crea l'objecte i se li pasa referencia
			joc.setScreen(new JocScreen(joc));
		}else {
//			joc.camera.update();
//			joc.batch.setProjectionMatrix(joc.camera.combined);
						
			int ample = Gdx.graphics.getWidth();
			int alt = Gdx.graphics.getHeight();
			
			joc.batch.begin();
			joc.batch.draw(logo, 0, 0, ample, alt);			
			joc.batch.end();
		}		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		logo.dispose();
	}


}